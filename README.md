# UPC\+\+ Extras: Examples and Extensions for UPC\+\+ #

This repository provides example codes and library extensions for UPC++.
The codes in this repository require the latest version of the
[UPC++ library](http://upcxx.lbl.gov).

UPC++ is a parallel programming library for developing C++ applications with
the Partitioned Global Address Space (PGAS) model.  UPC++ has three main
objectives:

* Provide an object-oriented PGAS programming model in the context of the
  popular C++ language

* Expose useful asynchronous parallel programming idioms unavailable in
  traditional SPMD models, such as remote function invocation and
  continuation-based operation completion, to support complex scientific
  applications
 
* Offer an easy on-ramp to PGAS programming through interoperability with other
  existing parallel programming systems (e.g., MPI, OpenMP, CUDA)

## Contents overview

### `examples/`
  
This directory contains various example codes using UPC++.
See the README file in each subdirectory for further details and instructions.

### `extensions/`

This directory contains library extensions to UPC++, written in terms of UPC++ APIs.
See the README file in each subdirectory for further details and instructions.

## Legal terms

UPC++ Extras is an optional component of the [UPC++ library](http://upcxx.lbl.gov),
which is distributed separately for convenience of packaging. 
As such, this software is subject to the same copyright notice and licensing agreement, 
which is available in [LICENSE.txt](LICENSE.txt)

## ChangeLog

### 2019-06-19: bfd492f
* Initial public version 


