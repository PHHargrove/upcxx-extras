This directory contains an example of Cannon's algorithm written in UPC++.

Cannon's algorithm is a scalable parallel implementation of matrix-matrix
multiplication which exchanges more computation for less communication than a
naive, classical implementation might use. Details on Cannon's algorithm can be
found on its Wikipedia page (https://en.wikipedia.org/wiki/Cannon%27s_algorithm).

This implementation depends on:

  - A working UPC++ v1.0 implementation (2019.3.0 or later), with the upcxx
    compiler in the user's PATH
  - A BLAS and CBLAS installation, with the environment variables BLAS_INSTALL
    and CBLAS_INSTALL set appropriately.

The provided Makefile is just one example of building this application on a
Linux cluster, given some external include directories and libraries containing
the definition and implementation of a third-party C/BLAS library. This Makefile
will likely need to be tweaked depending on your system's configuration.

This example can be optionally build to use CUDA devices to accelerate the
computation in Cannon's algorithm by offloading the GEMM kernel to the CUBLAS
library. An example Makefile rule (cannon_cuda) is included in the provided
Makefile. This version of the code leverages UPC++'s memory kinds support to
perform direct copies between CUDA devices on different nodes using upcxx::copy,
and therefore requires a minimum of UPC++ v1.0 2019.3.0.
