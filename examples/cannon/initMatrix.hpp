#ifndef _INIT_MATRIX_HPP
#define _INIT_MATRIX_HPP

/*
 * Uses a CUDA kernel to initialize the contents of A and B, two device arrays
 * on a CUDA device.
 */
void initMatrixOnDevice(double *A, double *B, int M, double Ainit,
        double Binit);

#endif
