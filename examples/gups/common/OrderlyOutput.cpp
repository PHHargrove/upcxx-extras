#ifndef _OrderlyOutput_
#define _OrderlyOutput_
#include <assert.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <cstring>
#include <string>
#include <upcxx/upcxx.hpp>

using namespace std;
using namespace upcxx;

void OrderlyOutput(ostringstream& s1){
   intrank_t myrank = upcxx::rank_me();
   intrank_t nranks = upcxx::rank_n();

   string str = s1.str();
   int len = str.length()+1;
   char * cstr = new char [len];
   assert(cstr);
   strcpy (cstr, str.c_str());  // cstr now contains a c-string copy of str

//   int maxLen = reduce_all(len,  [](const int & a, const int & b){ return std::max(a,b);}).wait();
   int maxLen = reduce_all(len, upcxx::op_fast_max).wait();
   global_ptr<char> sbuff = nullptr;
   if (!myrank){
       sbuff  = new_array<char>(maxLen*nranks);
       cout <<  endl << cstr;
   }
   upcxx::barrier();
   sbuff = upcxx::broadcast(sbuff,0).wait();
   assert(len <= maxLen);
   if (myrank>0){
       rput(cstr,sbuff+myrank*maxLen,len).wait();
   }

   upcxx::barrier();
   if (!myrank){
       assert(sbuff.is_local());
       char *sp = sbuff.local();
       for (int i=1; i<nranks; i++){
           cout << sp+maxLen*i;
       }
   }

   upcxx::barrier();

   delete[ ] cstr;
   if (!myrank){
       delete_array(sbuff);
   }
}
#endif
