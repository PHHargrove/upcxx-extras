#include <stdlib.h>
#include <inttypes.h>
#include <getopt.h>
#include <assert.h>
#include "guppie.hpp"

using namespace std;

/********************************
 divide up total size (loop iters or space amount) in a blocked way
*********************************/
void Block(int mype, int npes,
           const int64& totalsize,
           int64& start, int64& stop, int64& size)
{
    int64 div;
    int64 rem;

    div = totalsize / npes;
    rem = totalsize % npes;

    if (mype < rem)
    {
        start = mype * (div + 1);
        stop   = start + div;
        size  = div + 1;
    }
    else
    {
        start = mype * div + rem;
        stop  = start + div - 1;
        size  = div;
    }
}

