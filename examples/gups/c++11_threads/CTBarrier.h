/*
 * CSE 160 Assignment #2
 * William Gahr, Michael Max Hughes
 * 10/29/2013
 * Based on:
 * Many Body Simulation Starter code
 * Scott B. Baden, CSE Dept, UCSD
 * 10/13/2013
 */

#ifndef _BARRIER_H
#define _BARRIER_H

#include <atomic>

#define LINE_SIZE 64

using namespace std;


// base class for barriers
class Barrier
{
        public:
            virtual void bsync(int TID) = 0;
};

// Based on the combining tre barrier algorithm described in
// Parallel Computer Architecutre"
// by Culler, Singh and Gupta
class CombiningTreeBarrier : public Barrier
{
    atomic<int> **arrival_counters;
    atomic_flag **release_flags;
    int _NT;

public:

    //
    // barrier constructor
    // NT is the total number of threads that will be using it
    //
    CombiningTreeBarrier(int NT = 2) {

        // NT-1 elements are needed
        // this is the amount of nodes in a tree above the leaves with NT leaves

        // the tree is stored in a vector with the root at index 0,
        // its children at index 1-2, the next level at 3-6, etc

        arrival_counters = new atomic<int>*[NT-1];
        release_flags = new atomic_flag*[NT-1];

        // arrival counters start clear
        // release flags start set
        for (int i = 0; i < NT-1; i++) {
            arrival_counters[i] = new atomic<int>[LINE_SIZE];
            arrival_counters[i][0].store(0);
            release_flags[i] = new atomic_flag[LINE_SIZE];
            release_flags[i][0].test_and_set();
        }

        _NT = NT;
    }

    ~CombiningTreeBarrier(){
        delete [] arrival_counters;
        delete [] release_flags;
    }

    //
    // Logarithmic time barrier
    // Based on the combining tree barrier algorithm described in
    // "Parallel Computer Architecture" 9/10/97 by
    // David Culler, Jaswinder Pal Singh and Anoop Gupta
    //
    void bsync(int TID) {

        if (_NT == 1)
            return;
        // this corresponds to where the leaf would be in the tree
        int leafIdx = TID+_NT-1;

        // calculate the index of this node's parent
        int nodeIdx = (leafIdx-1) / 2;

        // wait for the previous barrier to complete
        while (arrival_counters[nodeIdx][0] == 2) {
            ; // do nothing
        }

        while (++arrival_counters[nodeIdx][0] == 2) {
            // if counter is 2, another child of this node already arrived
            // if we're at the root node, we're done and can start releasing threads
            if (nodeIdx == 0) {
                // release the root node and continue on
                release_flags[0][0].clear();
                return;
            }

            // continue up the tree to this node's parent
            nodeIdx = (nodeIdx-1) / 2;
        }

        // wait until we can acquire the flag for our node
        while (release_flags[nodeIdx][0].test_and_set()) {
            ; // do nothing
        }

        // reset the arrival counter of this node
        //printf("TID %i clearing arrival counter %i\n", TID, nodeIdx);
        arrival_counters[nodeIdx][0].store(0);

        // release children if there are any
        int childIdx = 2*nodeIdx + 1; // left child
        if (childIdx < (_NT-1)) {
            release_flags[childIdx][0].clear();

            childIdx++; // right child
            if (childIdx < (_NT-1))
                release_flags[childIdx][0].clear();
        }
      
    }
};
#endif
