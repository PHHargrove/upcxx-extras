#include <stdio.h>
#include <iostream>
#include <chrono>
#include <stdlib.h>
#include <string.h>
#include <cuda.h>
#include "config.hpp"
#include "utils.hpp"
#include "gpuFuns.hpp"
#include "ghostRegion.hpp"

#if UPCXX_VERSION < 20190300
#error This test requires UPC++ 2019.3.0 or newer (cuda support)
#endif

using namespace std;

void cmdLine(int argc,char **argv, int rank, int numprocs, int& Nx, int& Ny,
        int& Nz, int& nIters, int& px, int& py, int& pz);
double*** Alloc3D(int nx, int ny, int nz, char *mesg);
double  resid7(double* U, double* B,const int nx, const int ny, const int nz,
        const int Nx, const int Ny, const int Nz);
double GFlops(const double time, const int Nx,const int Ny, const int Nz,
        const int iters);
void printTOD(const char *mesg);
void OrderlyOutput(ostringstream& s1);
void initMesh(int *n, int *p, int *r, double *U, double *Un, double *B);
void PrintUsage(const char *const program);

// A sparse table with expected results for various problem configurations
typedef struct _Jacobi3DSolution {
    int Nx;
    int Ny;
    int Nz;
    int iters;
    double expected_resid;
} Jacobi3DSolution;

static Jacobi3DSolution expectedSolutions[] = {
    {128, 128, 128, 1000, 1.390334e-03},
    {256, 256, 256, 1000, 1.026033e-03},
    {512, 512, 512, 1000, 7.338118e-04}
};

extern void GPU_jac3d_driver(dim3& dimGrid, dim3& dimBlock, int by, int nx,
        int ny, int nz, int pitch, double *d_U, double *d_Un, double *d_B);

int lastCudaErr(int rank, const char *mesg) {
    cudaError_t c_err = cudaGetLastError();
    ostringstream sl;

    int gerr = upcxx::reduce_all((int)c_err, upcxx::op_fast_bit_or).wait();

    if (!gerr){
        if (!rank){
            if (mesg != NULL) sl << mesg << ": ";
            sl << "No CUDA Errors!\n";
        }
    } else {
        if (mesg != NULL) sl << mesg << ": ";
        sl << "CUDA error from rank " << rank << ": " <<
            cudaGetErrorString(c_err) << endl;
    }
    OrderlyOutput(sl);
    return gerr;
}

static void checkErr(int Nx, int Ny, int Nz, int nIters, double resid) {
    for (int i = 0; i < sizeof(expectedSolutions) / sizeof(expectedSolutions[0]); i++) {
        Jacobi3DSolution *sol = &expectedSolutions[i];
        if (sol->Nx == Nx && sol->Ny == Ny && sol->Nz == Nz &&
                sol->iters == nIters) {
            if (fabs(sol->expected_resid - resid) > 1e-3) {
                printf("FAIL\n");
            } else {
                printf("SUCCESS\n");
            }
            return;
        }
    }

    printf("WARNING: Unable to validate residual, unsupported problem size.\n");
    printf("SUCCESS\n");
}

int main(int argc,char **argv)
{
    double *d_U=NULL, *d_Un=NULL, *d_B=NULL;
    double *U=NULL, *Un=NULL, *B;
   // Configuration variables
    int Nx,Ny,Nz, nIters;
    int px, py, pz;
    int pitch;
    size_t pitch_bytes;
    int nx, ny, nz;
    int numprocs, rank;
    int rankx, ranky, rankz;

    upcxx::init();
    numprocs = upcxx::rank_n();
    rank = upcxx::rank_me();

    cmdLine(argc,argv,rank,numprocs,Nx,Ny,Nz,nIters,px,py,pz);

    nx = Nx/px, ny = Ny/py, nz = Nz/pz;

    rankz = rank / (px*py);
    ranky = (rank % (px*py)) / px;
    rankx = (rank % (px*py)) % px;

    // Allocate and clear the timers
    // These store the time by iteration.
    double *tI = new double[nIters];
    if ( !tI ) fprintf(stderr, "Unable to allocate timer array on rank %d\n",rank);
    for (int i=0; i<nIters; i++) tI[i] = 0.0;

    if (!rank){
        cout << "\n\n7-Point Point Jacobi with [Nx Ny Nz] = [";
        cout << Nx << " " << Ny << Nz << "]\n";

#ifdef POISSON
        cout << "Solving Poisson's Equation\n";
#else
        cout << "Solving Laplace's Equation\n";
#endif
        cout << "Using DOUBLE Precision\n";
        cout << "iterations:  " << nIters << endl << endl;
    }
    // Report device attributes
    if (!rank)
        ReportDevice();

    upcxx::barrier();

    gpuErrchk(cudaMallocHost((void**)&U,sizeof(double)*(nx+2)*(ny+2)*(nz+2)));
    gpuErrchk(cudaMallocHost((void**)&Un,sizeof(double)*(nx+2)*(ny+2)*(nz+2)));
    gpuErrchk(cudaMallocPitch((void **)&d_U, &pitch_bytes,
                sizeof(double)*(nx+2), (ny+2)*(nz+2)));
    gpuErrchk(cudaMallocPitch((void **)&d_Un, &pitch_bytes,
                sizeof(double)*(nx+2), (ny+2)*(nz+2)));
#ifdef POISSON
    gpuErrchk(cudaMallocHost((void**)&B,sizeof(double)*(nx+2)*(ny+2)*(nz+2)));
    gpuErrchk(cudaMallocPitch((void **)&d_B, &pitch_bytes, sizeof(double)*(nx+2),
                (ny+2)*(nz+2)));
#endif
    pitch = pitch_bytes/sizeof(double);
    int nxyz[3]; nxyz[0] = nx; nxyz[1] = ny; nxyz[2] = nz;
    int pxyz[3]; pxyz[0] = px; pxyz[1] = py; pxyz[2] = pz;
    int rxyz[3]; rxyz[0] = rankx; rxyz[1] = ranky; rxyz[2] = rankz;

    initMesh(nxyz, pxyz, rxyz, U, Un, B);

#ifdef PRINTMESH
    printMesh(nxyz,U, rank);
#endif

    // Copy down
    cudaMemcpy2D(d_U, pitch_bytes, U, sizeof(double)*(nx+2),
            sizeof(double)*(nx+2), (ny+2)*(nz+2), cudaMemcpyHostToDevice);
    cudaMemcpy2D(d_Un, pitch_bytes, U, sizeof(double)*(nx+2),
            sizeof(double)*(nx+2), (ny+2)*(nz+2), cudaMemcpyHostToDevice);
#ifdef POISSON
    cudaMemcpy2D(d_B, pitch_bytes, B, sizeof(double)*(nx+2),
            sizeof(double)*(nx+2), (ny+2)*(nz+2), cudaMemcpyHostToDevice);
#endif

    gpuErrchk(cudaDeviceSynchronize());

    int bx = ((nx + 2) + BLOCK_X - 1) / BLOCK_X;
    int by = ((ny + 2) + BLOCK_Y - 1) / BLOCK_Y;
    int bz = ((nz + 2) + NPOINTS_Z - 1) / NPOINTS_Z;

    // Set up the execution configuration
    dim3 dimGrid(bx,by*bz);
    dim3 dimBlock(BLOCK_X,BLOCK_Y);

    ghostRegion<double> *Ghosts = new ghostRegion<double>(nxyz, pxyz, rxyz,
            pitch_bytes, d_U, d_Un, dimGrid, dimBlock);
    Ghosts->allocBuffs();

    upcxx::barrier();
    if (!rank){
        cout << "DimGrid  = " << dimGrid.x << " " << dimGrid.y << " " << dimGrid.z << endl;
        cout << "DimBlock  = " << dimBlock.x << " " << dimBlock.y << " " << dimBlock.z << endl;
        cout << "px = " << px << "   py = " << py << "   pz = " << pz << endl;
        cout << "nx = " << nx << "   ny = " << ny << "   nz = " << nz << endl;
        cout << "pitch = " << pitch << endl;
        printTOD("Run begins");
    }

    std::chrono::steady_clock::time_point startIter =
        std::chrono::steady_clock::now();

    gpuErrchk(cudaDeviceSynchronize());

    upcxx::barrier();

    for (int i = 1; i <= nIters; i++) {

        GPU_jac3d_driver(dimGrid, dimBlock, by, nx, ny, nz, pitch, d_U,
                d_Un, d_B);
        gpuErrchk(cudaDeviceSynchronize());
        gpuErrchk(cudaGetLastError());

        Ghosts->fillGPU();
        gpuErrchk(cudaDeviceSynchronize()); 
        gpuErrchk(cudaGetLastError());

        std::swap(d_U, d_Un);
    }

    upcxx::barrier();

    double timeIter = std::chrono::duration<double>(
            std::chrono::steady_clock::now() - startIter).count();


    std::chrono::steady_clock::time_point startCopyback =
        std::chrono::steady_clock::now();

    gpuErrchk(cudaMemcpy2D(U, sizeof(double)*(nx+2),d_U, pitch_bytes,
                sizeof(double)*(nx+2), (ny+2)*(nz+2),cudaMemcpyDeviceToHost) );
    double t_copyBack = std::chrono::duration<double>(
            std::chrono::steady_clock::now() - startCopyback).count();

    // *** Residual for the Laplace equation not yet implemented

    double resid = resid7(U, B, nx+2, ny+2, nz+2, Nx, Ny, Nz);

    if (rank==0) {
        // ***  Copyback time measurement turned off. Not sure it is accurate.
        //      printf("\nCopy Unew to host: %e (s) \n",t_copyback);
        printf("\nTotal wall-clock time (sec) : %e\n", timeIter);
        printf("Per iteration time    (sec) : %e\n",timeIter/(double)nIters);
        printf("GFLOPS                      : %f\n" , GFlops(timeIter,Nx,Ny,Nz,nIters));
        printf("L2Norm of the Error         : %f\n" , resid);
        printf("     Nx    Ny     Nz   Px  Py  Pz     It       Time    Gflops   PREC  P/L   Err\n"); // Abbreviated output for data scraping
        printf("#> ");
        printf("%4d ",Nx);
        printf(" %4d  ",Ny);
        printf(" %4d  ",Nz);
        printf("%3d",px);
        printf(" %3d",py);
        printf(" %3d ",pz);
        printf("  %4d ",nIters);
        printf("  %9.2e",timeIter);
        printf("  %7.2e ", GFlops(timeIter,Nx,Ny,Nz,nIters));
        printf("  DP ");
#ifdef POISSON
        printf("   P");
#else
        printf("   L");
#endif
        printf("  %e\n", resid);
        checkErr(Nx, Ny, Nz, nIters, resid);
        printTOD("\nRun Completes");
    }

    lastCudaErr(rank, NULL);

    // Release GPU and CPU memory
    cudaFree(d_U); cudaFree(d_Un); 

#ifdef POISSON
    cudaFree(d_B);
#endif

    delete [] tI;
    upcxx::finalize();
    return EXIT_SUCCESS;
}
