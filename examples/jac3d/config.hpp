#ifdef _CONFIG_H
#else
#define _CONFIG_H
// define precision 
#define POISSON

// define kernel block size
#define BLOCK_X 64 
#define BLOCK_Y 16 
#define NPOINTS_Z 256 

#endif
