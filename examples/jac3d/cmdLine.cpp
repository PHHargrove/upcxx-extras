/****************************************************************
*								*
*  Parse command line						*
*								*
* Author:  Scott Baden						*
****************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>

// Default values

const int def_Nx = 128;
const int def_Ny = 128;
const int def_Nz = 128;
const int def_N = 128;
const int def_nIters = 10;
const int def_px = 1;
const int def_py = 1;
const int def_pz = 1;

void PrintUsage(const char *const program);

extern void cmdLine(int argc, char **argv, int rank, int numprocs,
	     int& Nx,int& Ny,int& Nz, int& nIters,
	     int& px, int& py, int& pz)
{

    // Fill in default values
    Nx = def_Nx;
    Ny = def_Ny;
    Nz = def_Nz;
    nIters = def_nIters;
    px = def_px;
    py = def_py;
    pz = def_pz;
    int N = def_N;

    extern char *optarg;
    static struct option long_options[] = {
        {"Nx", required_argument, NULL, 'X'},
        {"Ny", required_argument, NULL, 'Y'},
        {"Nz", required_argument, NULL, 'Z'},
        {"i",  required_argument, NULL, 'i'},
        {"px", required_argument, NULL, 'x'},
        {"py", required_argument, NULL, 'y'},
        {"pz", required_argument, NULL, 'z'},
        {"N",  required_argument, NULL, 'N'},
        {NULL, 0, 0, 0}
    };

    int N_provided = 0;
    int N_xyz_provided = 0;
    int p_xyz_provided = 0;

    // Parse the command line arguments -- if something is not kosher, die
    // Override with command-line input parameters (if any)
    int c;
    while ((c=getopt_long_only(argc, argv,"", long_options, NULL)) != -1)
    {
        switch (c) {
            case 'X':
                Nx = atoi(optarg);
                N_xyz_provided = 1;
                break;
            case 'Y':
                Ny = atoi(optarg);
                N_xyz_provided = 1;
                break;
            case 'Z':
                Nz = atoi(optarg);
                N_xyz_provided = 1;
                break;
            case 'i':
                nIters = atoi(optarg); break;
            case 'x':
                px = atoi(optarg);
                p_xyz_provided = 1;
                break;
            case 'y':
                py = atoi(optarg);
                p_xyz_provided = 1;
                break;
            case 'z':
                pz = atoi(optarg);
                p_xyz_provided = 1;
                break;
            case 'N':
                N = atoi(optarg);
                N_provided = 1;
                break;
            case '?':
            default:
                if (rank == 0) {
                    PrintUsage(argv[0]);
                }
                exit(-1);
        }
    }

    if (N_provided && N_xyz_provided) {
        if (rank == 0) {
            fprintf(stderr, "Both -N and -Nx/-Ny/-Nz were provided as command "
                    "line arguments. Please use one way to specify domain "
                    "size.\n");
            PrintUsage(argv[0]);
        }
        exit(-1);
    }

    if (N_provided) {
        // Overwrite with provided N
        Nx = Ny = Nz = N;
    }

    if (!p_xyz_provided) {
        // If -px/-py/-pz were not set, try auto-selecting a rank configuration
        int rank_config[3] = {px, py, pz};

        int rank_index = 0;
        while (rank_config[0] * rank_config[1] * rank_config[2] < numprocs) {
            // Round robin doubling of process dimensions
            rank_config[rank_index] *= 2;
            rank_index = (rank_index + 1) % 3;
        }

        px = rank_config[0];
        py = rank_config[1];
        pz = rank_config[2];
    }

    /*
     * Nx, Ny, Nz, are the dimensions of the problem space.
     *
     * px, py, pz are the number of partitions/processors in each dimension that
     * the partition space will be divided into
     *
     * Assert that Nx, Ny, Nz are divided evenly by partition size over the 3
     * dimensions
     */
    int validation_err = 0;
    if (px * py * pz != numprocs) {
        if (rank == 0) {
            fprintf(stderr, "ERROR # of processors (%d) does not equal "
                    "px * py * pz (%d).\n", numprocs, px * py * pz);
        }
        validation_err = 1;
    }

    if (Nx % px != 0) {
        if (rank == 0) {
            fprintf(stderr, "px (%d) must evenly divide Nx (%d).\n", px, Nx);
        }
        validation_err = 1;
    }

    if (Ny % py != 0) {
        if (rank == 0) {
            fprintf(stderr, "py (%d) must evenly divide Ny (%d).\n", py, Ny);
        }
        validation_err = 1;
    }

    if (Nz % pz != 0) {
        if (rank == 0) {
            fprintf(stderr, "pz (%d) must evenly divide Nz (%d).\n", pz, Nz);
        }
        validation_err = 1;
    }

    if (validation_err) {
        if (rank == 0) {
            PrintUsage(argv[0]);
        }
        exit(-1);
    }
}

/****************************************************************
* void PrintUsage(const char *const program, 			*
*		  const char *const option)			*
*								*
* Prints out the command line options 				*
*****************************************************************/

void PrintUsage(const char *const program)
{
    fprintf(stderr, "\nusage: %s [--Nx <meshpoints>] [--Ny <meshpoints>] "
            "[--Nz <meshpoints>] [-N <meshpoints>] [--i <iterations>] "
            "[--px <x processors] [--py <y processors>] [--pz <z processors]\n",
            program);

   fprintf(stderr,"\t-Nx,Ny,Nz        <integer>  problem domain size\n");
   fprintf(stderr,"\t-N               <integer>  set same domain size in all 3 dimensions\n");
   fprintf(stderr,"\t-i               <integer>  # iterations\n");
   fprintf(stderr,"\t-px              <integer>  number of processors on x axis\n");
   fprintf(stderr,"\t-py              <integer>  number of processors on y axis\n");
   fprintf(stderr,"\t-pz              <integer>  number of processors on z axis\n");
}
