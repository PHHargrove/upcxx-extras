#ifdef _GHOSTREGION_HPP
#else
#define _GHOSTREGION_HPP

#include <assert.h>
#include <upcxx/upcxx.hpp>

using namespace upcxx;

static const unsigned int ND = 3;

template<typename V>
class ghostRegion{

private:
    upcxx::global_ptr<V> h_sndBuff[ND][2];
    upcxx::dist_object<upcxx::global_ptr<V>> *dobj_sndBuffs[ND][2];
    upcxx::global_ptr<V> remote_h_sndBuff[ND][2];

    double *h_rcvBuff[ND][2];
    V *d_sndBuff[ND][2], *d_rcvBuff[ND][2];
// The shape of the local mesh, with origin 0
    int d[ND];
    int p[ND];
    int _rank;
    int r[ND];
    int s[ND]; 	// Strides for neighbor rank offsets
    upcxx::future<> request_r[ND][2];
    static const unsigned X = 0, Y = 1, Z = 2;
    dim3 dimGrid, dimBlock;
    int _pitch;
    size_t _pitch_bytes;
    int neighbors[ND][2];

    int g_nelt[2], g_len[2], g_displ[2], s_len[3], scat_len[3];
    size_t g_clen[2];

    V *d_U, *d_Un;

public:

    ~ghostRegion() {
        // Free Host and Device buffers
        for (int i=0; i<2; i++){
            for (int d=0; d<2; d++){
                upcxx::delete_array(h_sndBuff[d][i]);
                delete h_rcvBuff[d][i];
                cudaFree(d_sndBuff[d][i]);
                cudaFree(d_rcvBuff[d][i]);
            }
        }
    }

    ghostRegion(int nk[ND], int pk[ND], int myrank_k[ND], size_t pitch_bytes,
            V *_d_U, V *_d_Un, dim3 _dimGrid, dim3 _dimBlock) {
        _rank = upcxx::rank_me();

        /*
         * s is the stride (measured in PEs) that it takes to get to a neighbor in
         * each dimension.
         *
         * pk is the number of PEs in each dimension.
         */
        s[X] = 1;
        s[Y] = pk[X];
        s[Z] = pk[X]*pk[Y];

        for (int i=0; i<ND; i++){
            d[i] = nk[i];
            p[i] = pk[i];
            r[i] = myrank_k[i]; // My offset in PEs in a given dimension
            for (int j=0; j<1; j++){
                d_sndBuff[i][j] = NULL;
                d_rcvBuff[i][j] = NULL;
            }
            // left/right, front/rear, down/up
            neighbors[i][0] =  (r[i] > 0       ) ? (_rank-s[i]) : -1;
            neighbors[i][1] =  (r[i] < (p[i]-1)) ? (_rank+s[i]) : -1;
        }
        _pitch_bytes = pitch_bytes;
        _pitch = _pitch_bytes/sizeof(V);
        d_U = _d_U;
        d_Un = _d_Un;
        dimGrid = _dimGrid;
        dimBlock = _dimBlock;
        g_nelt[0] = (d[Z]+2)*(d[Y]+2); g_nelt[1] = d[Z]+2;
        g_len[0] = 1; g_len[1] = d[X]+2;
        g_displ[0]  = _pitch ; g_displ[1] = _pitch*(d[Y]+2);
        s_len[0] = (d[Z]+2)*(d[Y]+2);
        s_len[1] = (d[Z]+2)*(d[X]+2);
        s_len[2] = (d[X]+2)*(d[Y]+2);
        g_clen[0]= sizeof(V)*s_len[0];
        g_clen[1]= sizeof(V)*s_len[1];
    }

   int getPitch(){return _pitch;}
   int getPitch_bytes(){return _pitch_bytes;}

   void allocBuffs(){
       for (int i = 0; i <= 1; i++){
           // Host buffers
           h_rcvBuff[X][i] = new V[(d[Y]+2)*(d[Z]+2)];
           h_rcvBuff[Y][i] = new V[(d[X]+2)*(d[Z]+2)];
           h_rcvBuff[Z][i] = new V[(d[X]+2)*(d[Y]+2)];

           h_sndBuff[X][i] = upcxx::new_array<V>((d[Y]+2)*(d[Z]+2));
           h_sndBuff[Y][i] = upcxx::new_array<V>((d[X]+2)*(d[Z]+2));
           h_sndBuff[Z][i] = upcxx::new_array<V>((d[X]+2)*(d[Y]+2));

           dobj_sndBuffs[X][i] = new upcxx::dist_object<upcxx::global_ptr<V>>(h_sndBuff[X][i]);
           dobj_sndBuffs[Y][i] = new upcxx::dist_object<upcxx::global_ptr<V>>(h_sndBuff[Y][i]);
           dobj_sndBuffs[Z][i] = new upcxx::dist_object<upcxx::global_ptr<V>>(h_sndBuff[Z][i]);

           // Device buffers
           gpuErrchk(cudaMalloc((void**)&d_sndBuff[X][i],sizeof(V)*(d[Y]+2)*(d[Z]+2)));
           gpuErrchk(cudaMalloc((void**)&d_sndBuff[Y][i],sizeof(V)*(d[X]+2)*(d[Z]+2)));
           gpuErrchk(cudaMalloc((void**)&d_sndBuff[Z][i],sizeof(V)*(d[X]+2)*(d[Y]+2)));
           gpuErrchk(cudaMalloc((void**)&d_rcvBuff[X][i],sizeof(V)*(d[Y]+2)*(d[Z]+2)));
           gpuErrchk(cudaMalloc((void**)&d_rcvBuff[Y][i],sizeof(V)*(d[X]+2)*(d[Z]+2)));
           gpuErrchk(cudaMalloc((void**)&d_rcvBuff[Z][i],sizeof(V)*(d[X]+2)*(d[Y]+2)));
       }

       if (r[X] != 0){
           // Get the positive/right send buf of my left/negative X neighbor
           remote_h_sndBuff[X][0] = dobj_sndBuffs[X][1]->fetch(neighbors[X][0]).wait();
       }

       if (r[X] != p[X]-1){
           // Get the negative/left send buf of my right/positive X neighbor
           remote_h_sndBuff[X][1] = dobj_sndBuffs[X][0]->fetch(neighbors[X][1]).wait();
       }

       if (r[Y] != 0){
           // Get the positive/right send buf of my left/negative Y neighbor
           remote_h_sndBuff[Y][0] = dobj_sndBuffs[Y][1]->fetch(neighbors[Y][0]).wait();
       }

       if (r[Y] != p[Y]-1){
           // Get the negative/left send buf of my right/positive Y neighbor
           remote_h_sndBuff[Y][1] = dobj_sndBuffs[Y][0]->fetch(neighbors[Y][1]).wait();
       }

       if (r[Z] != 0){
           // Get the positive/right send buf of my left/negative Y neighbor
           remote_h_sndBuff[Z][0] = dobj_sndBuffs[Z][1]->fetch(neighbors[Z][0]).wait();
       }

       if (r[Z] != p[Z]-1){
           remote_h_sndBuff[Z][1] = dobj_sndBuffs[Z][0]->fetch(neighbors[Z][1]).wait();
       }

       upcxx::barrier();
   }

   void fillHostBuffersFromDevice() {
       if(r[X] != 0){
           gather_GPU(d_sndBuff[X][0], &d_Un[1], g_nelt[0], g_len[0], g_displ[0],
                   dimGrid, dimBlock, _pitch, d[X], d[Y], d[Z]);
           gpuErrchk(cudaMemcpy(h_sndBuff[X][0].local(), d_sndBuff[X][0],
                       g_clen[0], cudaMemcpyDeviceToHost));
       }

       if(r[X] != p[X]-1){
           gather_GPU(d_sndBuff[X][1], &d_Un[d[X]], g_nelt[0], g_len[0],
                   g_displ[0], dimGrid, dimBlock, _pitch, d[X], d[Y], d[Z]);
           gpuErrchk(cudaMemcpy(h_sndBuff[X][1].local(), d_sndBuff[X][1],
                       g_clen[0], cudaMemcpyDeviceToHost));
       }

       if(r[Y]!=0){
           gather_GPU(d_sndBuff[Y][0], &d_Un[_pitch], g_nelt[1], g_len[1],
                   g_displ[1], dimGrid, dimBlock, _pitch, d[X], d[Y], d[Z]);
           gpuErrchk(cudaMemcpy(h_sndBuff[Y][0].local(), d_sndBuff[Y][0],
                       g_clen[1], cudaMemcpyDeviceToHost));
       }

       if(r[Y]!=p[Y]-1){
           gather_GPU(d_sndBuff[Y][1], &d_Un[(d[Y])*_pitch], g_nelt[1], g_len[1],
                   g_displ[1], dimGrid, dimBlock, _pitch, d[X], d[Y], d[Z]);
           gpuErrchk(cudaMemcpy(h_sndBuff[Y][1].local(), d_sndBuff[Y][1],
                       g_clen[1], cudaMemcpyDeviceToHost));
       }

       if(r[Z]!=0){
           gpuErrchk(cudaMemcpy2D(h_sndBuff[Z][0].local(), sizeof(V)*(d[X]+2),
                       &d_Un[_pitch*(d[Y]+2)], _pitch_bytes, sizeof(V)*(d[X]+2),
                       (d[Y]+2),cudaMemcpyDeviceToHost));
       }

       if(r[Z]!=p[Z]-1){
           gpuErrchk(cudaMemcpy2D(h_sndBuff[Z][1].local(), sizeof(V)*(d[X]+2),
                       &d_Un[(d[Z])*_pitch*(d[Y]+2)], _pitch_bytes,
                       sizeof(V)*(d[X]+2), (d[Y]+2),cudaMemcpyDeviceToHost));
       }
   }

   void fillGPU() {
       // Gather my GPU buffers into host memory
       fillHostBuffersFromDevice();

       /*
        * Ensure everyone has finished populating host buffers before we start
        * copying
        */
       upcxx::barrier();

       upcxx::future<> fut = upcxx::make_future();
       // Fetch 3D ghost region into my local buffers
       if(r[X] != 0){
           fut = upcxx::when_all(fut, upcxx::rget(remote_h_sndBuff[X][0],
                   h_rcvBuff[X][0], s_len[0]).then([&] {
                       gpuErrchk(cudaMemcpy(d_rcvBuff[X][0], h_rcvBuff[X][0],
                               g_clen[0], cudaMemcpyHostToDevice));
                       scatter_GPU(d_Un, d_rcvBuff[X][0], g_nelt[0], 1, _pitch,
                           dimGrid, dimBlock, _pitch, d[X], d[Y], d[Z]);
                   }));
       }

       if(r[X] != p[X]-1){
           fut = upcxx::when_all(fut, upcxx::rget(remote_h_sndBuff[X][1],
                   h_rcvBuff[X][1], s_len[0]).then([&] {
                       gpuErrchk(cudaMemcpy(d_rcvBuff[X][1], h_rcvBuff[X][1],
                               g_clen[0], cudaMemcpyHostToDevice));
                       scatter_GPU(&d_Un[d[X]+1], d_rcvBuff[X][1], g_nelt[0], 1,
                           _pitch, dimGrid, dimBlock, _pitch, d[X], d[Y], d[Z]);
                   }));
       }

       if(r[Y]!=0){
           fut = upcxx::when_all(fut, upcxx::rget(remote_h_sndBuff[Y][0],
                   h_rcvBuff[Y][0], s_len[1]).then([&] {
                       gpuErrchk(cudaMemcpy(d_rcvBuff[Y][0], h_rcvBuff[Y][0],
                               sizeof(V)*s_len[1], cudaMemcpyHostToDevice));
                       scatter_GPU(d_Un, d_rcvBuff[Y][0], d[Z]+2, d[X]+2,
                           _pitch*(d[Y]+2), dimGrid, dimBlock, _pitch, d[X],
                           d[Y], d[Z]);
                   }));
       }

       if(r[Y]!=p[Y]-1){
           fut = upcxx::when_all(fut, upcxx::rget(remote_h_sndBuff[Y][1],
                   h_rcvBuff[Y][1], s_len[1]).then([&] {
                       gpuErrchk(cudaMemcpy(d_rcvBuff[Y][1], h_rcvBuff[Y][1],
                               sizeof(V)*s_len[1], cudaMemcpyHostToDevice));
                       scatter_GPU(&d_Un[(d[Y]+1) * _pitch], d_rcvBuff[Y][1],
                           d[Z]+2, d[X]+2, _pitch*(d[Y]+2), dimGrid, dimBlock,
                           _pitch, d[X], d[Y], d[Z]);
                   }));
       }

       if(r[Z]!=0){
           fut = upcxx::when_all(fut, upcxx::rget(remote_h_sndBuff[Z][0],
                   h_rcvBuff[Z][0], s_len[2]).then([&] {
                       gpuErrchk(cudaMemcpy2D(d_Un, _pitch_bytes, h_rcvBuff[Z][0],
                               sizeof(V)*(d[X]+2), sizeof(V)*(d[X]+2), d[Y]+2,
                               cudaMemcpyHostToDevice));
                   }));
       }

       if(r[Z]!=p[Z]-1){
           fut = upcxx::when_all(fut, upcxx::rget(remote_h_sndBuff[Z][1],
                   h_rcvBuff[Z][1], s_len[2]).then([&] {
                       gpuErrchk(cudaMemcpy2D(&d_Un[(d[Z]+1)*_pitch*(d[Y]+2)],
                               _pitch_bytes, h_rcvBuff[Z][1], sizeof(V)*(d[X]+2),
                               sizeof(V)*(d[X]+2), (d[Y]+2),
                               cudaMemcpyHostToDevice));
                   }));
       }

       fut.wait();

       gpuErrchk(cudaDeviceSynchronize());

       // Ensure everyone has finished before we start computing
       upcxx::barrier();

       // We also swap in main()
       std::swap(d_U, d_Un);
   }
};
#endif
