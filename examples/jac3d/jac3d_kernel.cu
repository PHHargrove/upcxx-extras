#include "config.hpp"
#include "utils.hpp"
#include <cuda.h>

#define IOFF  1
#define JOFF (BLOCK_X+2)
#define KOFF (BLOCK_X+2)*(BLOCK_Y+2)

__global__ void GPU_jac3d(int by, int Nx,int Ny, int Nz, int pitch, double *d_U,double *d_Un, double *d_b){
    int   indg, indg_neighb, indg0;
    int   i, j, k, ind, ind_neighb, neighb, active;

    double up, down, down_neighb;
    double c = ((double) 1.0 / (double) 6.0);
    int planeOffset = Ny*pitch;

    // define local offsets in the region of calculation and neighbour -> each Block size + 2
    __shared__ double u1[KOFF];  // Each time load 1 planes to use locality


    // Set up indices for neigbourh region
    k    = threadIdx.x + threadIdx.y*BLOCK_X;
    neighb = k < 2*(BLOCK_X+BLOCK_Y+2);          // 2*(BLOCK_X+BLOCK_Y+2) first threads are used to load neighbour value

    if (neighb) {
          if (threadIdx.y<2) {               // y neighbours   (in the original cordinator)
                i = threadIdx.x;
                j = threadIdx.y*(BLOCK_Y+1) - 1;
          }else {                             // x neighbours   (in the original cordinator)
                i = (k%2)*(BLOCK_X+1) - 1;
                j =  k/2 - BLOCK_X - 1;
          }

          ind_neighb  = i+1 + (j+1) * JOFF;  // in the shared memory

          i      = i + blockIdx.x * BLOCK_X;   // global indices (in shared memory)
          j      = j + (blockIdx.y%by) * BLOCK_Y;
          indg_neighb = i + j * pitch + (blockIdx.y/by)*NPOINTS_Z*planeOffset;

          neighb   =  (i>=0) && (i<Nx) && (j>=0) && (j<Ny);
    }
    //set up indices for calculation block

    ind  = threadIdx.x + 1 + (threadIdx.y+1)*JOFF ; // to access shared memory

    i    = threadIdx.x + blockIdx.x * BLOCK_X;     // global indices to access the real memory
    j    = threadIdx.y + (blockIdx.y%by) * BLOCK_Y;
    indg = i + j * pitch+ (blockIdx.y/by)*NPOINTS_Z*planeOffset;

    active = (i<Nx) && (j<Ny);

    // do k planes sequentially

    if (active) {
          u1[ind] = d_U[indg];
          indg  = indg + planeOffset;
          down = d_U[indg];
    }

    if (neighb) {
          u1[ind_neighb] = d_U[indg_neighb];
          indg_neighb = indg_neighb + planeOffset;
          down_neighb = d_U[indg_neighb];
    }

    int north=ind+JOFF, south=ind-JOFF, east=ind+IOFF, west=ind-IOFF;
    int first = (blockIdx.y/by)*NPOINTS_Z+1;
    int last = (first+NPOINTS_Z < Nz-2?first+NPOINTS_Z: Nz-2) ;
    for (k= first;k<= last; k++) {
       // move two planes down and read in new plane k+1
       if (active) {
            indg0 = indg;
            indg  = indg + planeOffset;
            up = u1[ind];
            u1[ind]      = down;
            down = d_U[indg];
       }

       if (neighb) {
            indg_neighb = indg_neighb + planeOffset;
            u1[ind_neighb]      = down_neighb;
            down_neighb = d_U[indg_neighb];
       }

      int jacobi = (i>0) && (i<Nx-1) && (j>0) && (j<Ny-1)&& (k<Nz-1) ; // (k>0) && (k<Nz-1): no need

      __syncthreads();

      if (jacobi) {
            #ifdef POISSON
                    d_Un[indg0] = (u1[west] + u1[east] + u1[south] + u1[north]+ up + down - d_b[indg0]) * c;
                  }
            #else
                    d_Un[indg0] = ( u1[west] + u1[east] + u1[south] + u1[north]+ up + down) * c;
                  }
            #endif
      } //end for
}
