#include <cuda.h>
#include "config.hpp"

__global__ void gather_GPU(double* destination, double* source, int n_elements,
        int  length,int displacement, int pitch, int nx, int ny, int nz){
    int i    = threadIdx.x + blockIdx.x * BLOCK_X;     // global indices to access the real memory
    int j    = threadIdx.y + blockIdx.y * BLOCK_Y;
    int indg = j * pitch + i * (ny+2)*pitch;
    int active = (indg<displacement*n_elements);
    int destIdx=(indg/displacement)*length + indg%displacement;
    if(active) destination[destIdx] = source[indg];
}

__global__ void scatter_GPU(double* destination, double* source, int n_elements,int  length,int displacement, int pitch, int nx, int ny, int nz){
    int i    = threadIdx.x + blockIdx.x * BLOCK_X;     // global indices to access the real memory
    int j    = threadIdx.y + blockIdx.y * BLOCK_Y;
    int indg = j * pitch + i*(ny+2)*pitch;
    int active = (indg<displacement*n_elements) ;
    int sourceIdx=(indg/displacement)*length + indg%displacement;
    if(active) destination[indg] = source[sourceIdx];
}

void gather_GPU(double* destination, double* source, int n_elements,int length,
        int displacement, dim3 dimGrid, dim3 dimBlock, int pitch, int nx,
        int ny, int nz){  //length in units

    /*
     * The length parameter indicates the number of elements we are gathering
     * out of source and into destination at a time.
     *
     * When copying one element at a time, it is more efficient to use a GPU
     * kernel to perform this work in parallel (though bandwidth utilization
     * will likely still be low). Otherwise, we use cudaMemcpy to efficiently
     * gather multiple elements at once.
     */

    if (length==1) {
        gather_GPU<<<dimGrid, dimBlock>>>(destination, source, n_elements,
                length, displacement, pitch, nx, ny, nz);
    } else {
        int offset =0;
        for (int i=0; i< n_elements; i++) {
            cudaMemcpy(&destination[i*length], &source[offset],
                    length*sizeof(double),cudaMemcpyDeviceToDevice);
            offset += displacement;
        }
    }
}

void scatter_GPU(double* destination, double* source, int n_elements,
        int  length,int displacement, dim3 dimGrid, dim3 dimBlock, int pitch,
        int nx, int ny, int nz){
  if(length==1){
    scatter_GPU<<<dimGrid, dimBlock>>>(destination, source, n_elements, length,
            displacement, pitch, nx, ny, nz);
  }else{
    int offset = 0;
    for(int i=0; i< n_elements; i++){
        cudaMemcpy(&destination[offset], &source[i*length],
                length*sizeof(double), cudaMemcpyDeviceToDevice);
        offset += displacement;
    }
  }
}
