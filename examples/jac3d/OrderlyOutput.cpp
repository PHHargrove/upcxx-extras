// Two ways of printing out # neihbors aren't consistent
// one is right, the other is wrong

#include <iostream>
#include <assert.h>
#include <stdlib.h>
#include <sstream>
#include <cstring>
#include <string>
#include <upcxx/upcxx.hpp>

using namespace upcxx;
using namespace std;

void OrderlyOutput(ostringstream& s1){
    int myrank = upcxx::rank_me();
    int nranks = upcxx::rank_n();

    string str = s1.str();
    int len = str.length()+1;
    int gLen = upcxx::reduce_all(len, upcxx::op_fast_max).wait();

    global_ptr<char> cstr = upcxx::new_array<char>(gLen);
    memcpy(cstr.local(), str.c_str(), len);
    upcxx::dist_object<upcxx::global_ptr<char>> dobj(cstr);

    if (myrank == 0) {
        cout << str;

        for (int i = 1; i < nranks; i++) {
            global_ptr<char> remote_cstr = dobj.fetch(i).wait();
            upcxx::rget(remote_cstr, cstr.local(), gLen).wait();
            cout << cstr.local();
        }
    }
    cout << flush;

    upcxx::barrier();

    upcxx::delete_array(cstr);
}
